import json
import random
from unittest import TestCase

from pytest_check import check

from app import app
from database.db import db
from database.models import Insult
from utils.jokester import Jokester


class JokesterTest(TestCase):
    """This class is responsible for testing the Jokester utility class."""

    def test_get_all_jokes_from_catagory(self):
        catagory_choices = Jokester.get_catagories()
        random_catagory = random.choice(catagory_choices)
        jokes = Jokester.get_all_jokes_from_catagory(random_catagory)
        with check:
            for joke in jokes:
                assert joke["category"] == random_catagory
                assert joke["status"] == "Active"
                assert len(jokes) >= 1

    def test_get_random_joke(self):
        joke = Jokester.get_random_joke()
        with check:
            assert joke is not None
            assert isinstance(joke, dict)
            assert len(joke.keys()) == 7

    def test_catagorized_joke(self):
        joke = Jokester.get_categorized_joke("test - catagory")
        with check:
            assert joke["category"] == "test - catagory"
            assert joke["status"] == "Active"

    def test_get_specific_joke(self):
        test_joke_id = "63cc93abc56e9a2922344c75"
        joke = Jokester.get_specific_joke(test_joke_id)
        with check:
            assert joke["_id"]["$oid"] == test_joke_id
            assert joke["category"] == "test - catagory"
            assert joke["status"] == "Active"
