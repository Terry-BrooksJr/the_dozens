"""
This Module is the central location for all the routes that are used in the /insults endpoint.
"""

import json
import os

import bson
import limits.storage
import pendulum
import redis
from flask import current_app as app
from flask import request
from flask_cors import cross_origin
from flask_jwt_extended import get_jwt, jwt_required, verify_jwt_in_request
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_restx import Namespace, Resource, fields, reqparse
from database import Insult, User
from utils.errors import BannedUserError, InvaildTokenError, UnauthorizedError, errors
from utils.gatekeeper import SecurityGuard
from utils.jokester import Jokester
from extensions import  cache
import logging

#! Namespace Declaration
api = Namespace(
    "Insults",
    description="These endpoints allow for API Consumers to interact with the Insults Database. Currently, the API only supports GET requests.",
)

#!Namespace Related Models
GET_fields = api.model(
    "Insult",
    {
        "_id": fields.Arbitrary(description='A nested Object with a single nested key $oid a string representation of the MongoDB ObjectID.', example={"$oid": "5f9f5b9b0b9d9b0b9d0b9d0b"}),
        "explicit": fields.Boolean(description='Classification of the Joke to determine if it\'s content is considered explicit or otherwise NSFW. (Case Insensitive) <sub>If you feel a joke needs to be reclassified, please report the joke and provide the id</sub> ', example=True),
        "catagory": fields.String(description='Each joke is assigned to a category based on content. To get a full list of categories, see the documentation for the insult/categories endpoint', example="fat"),
        "content": fields.String(description='This the Content of the Joke', example="Yo momma is so fat... she went to the movies and sat next to everyone."),
        "added_on": fields.String(description="String representation of the date the joke was logged to the API database. Format: YYYY-MM-DD. (Auto Set By Server)", example="2020-11-01"),
        "added_by": fields.String(description="The Initials of the contributor and a unique identifier who added the joke to the API. (Registration Required - Planned Enhancement", example="TB12345"),
        "status": fields.String(description="The status of the Joke in the API. Valid Status include ['Active', 'Pending', 'Under Review', 'Deleted'] If a status is 'Deleted' please open and issue and provide the ID", example="Active"),
        
        
    },
)
catagory_model = api.model( "Catagories", {
    "Available Joke Categories": fields.Raw(cls_or_instance='instance', description="A list of all the available joke categories", example='["fat", "stupid", "snowflake", "..."]')
}
)
all_jokes_model = api.model("All Jokes", { 
                                          "All fat jokes": fields.Raw(cls_or_instance='instance', description="A list of all the available jokes in catagory passed as in the slug of the endpoint", example='''
                                                                      ["Yo momma is so fat... she went to the movies and sat next to everyone.",\n "Yo momma is so fat... she went to the movies and sat next to everyone.",\n "Yo momma is so fat... she went to the movies and sat next to everyone.", ...]'''),
    
})


POST_fields = api.model(
    "Insult (Post Method)",
    {
        "content": fields.String,
        "explicit": fields.Boolean,
        "catagory": fields.String,
        "bearer-token": fields.String,
    },
)


#! Top-Level Variables/Plugins
now = pendulum.now()

POST_parser = reqparse.RequestParser()
GET_parser = reqparse.RequestParser()
joke_categories = Jokester.get_catagories()
acceptable_boolean_responses = ["true", "false", True, False]
redis_limit_uri = os.getenv("RATE_LIMIT_REDIS_URI")
options = {}
redis_limit_storage = limits.storage.storage_from_string(
    redis_limit_uri, options=options
)

# limiter = Limiter(
#     app=app,
#     key_func=get_remote_address,
#   storage_uri="redis://localhost:6379",
#   storage_options={"socket_connect_timeout": 30},
#   strategy="fixed-window"
# )


# def # jokes_served():
# URI = os.getenv("REDIS_URI")
# redis_conn = redis.StrictRedis(
#     host=URI, port=6379, decode_responses=True
# )
# if redis_conn.get("utilization") is None:
#     redis_conn.set("utilization ", 0)
# else:
#     redis_conn.incr("utilization")
#     print('Count Incremented')



#!Request Parameters Designations
POST_parser.add_argument("content", type=str, required=True, location="form")
POST_parser.add_argument("explicit", type=str, required=True, location="form")
POST_parser.add_argument(
    "catagory",
    type=str,
    required=False,
    location="form",
    help="specify the jokes to a  category, if it doesn't fit to any choose 'snowflake'",
    choices=joke_categories,
)
POST_parser.add_argument(
    "bearer token",
    type=str,
    required=False,
    location="headers",
    help="Please ensure your have registered your email and password to receive a bearer token. See endpoint `/signup` for more information.",
)
GET_parser.add_argument(
    "explicit",
    type=str,
    location="args",
    help="Classification of the Joke to determine if it's content is considered explicit or otherwise NSFW.",
    choices=["true", "false"],
)
GET_parser.add_argument(
    "catagory",
    type=str,
    location="args",
    help="Jokes are catagorized based on content. To get a full list of categories, see the documentation for the `insult/categories` endpoint",
    choices=joke_categories,
)
GET_parser.add_argument(
    "metadata",
    type=str,
    location="args",
    help="To get the entire Joke, including metadata <sub>(date added, contributor, catagory, content, etc.)</sub>, set this to true. Default is false.",
    choices=["true", "false"],
)

#! ✅ GET Category Endpoint
# TODO - Add Cacheing
@api.route("insult/categories")
@api.doc("Get All Available Joke Categories")
class InsultCatagories(Resource):
    """Get All Available Joke Categories"""
    @cache.cached(timeout=900)
    @api.doc(model=catagory_model)
    def get(self):
        
        return {"Available Joke Categories": joke_categories}, 200


#! ✅ GET All Jokes in a Single Catagory Endpoint
# TODO - Add Pagination & Caching
@api.route("insults/<string:category>")
@api.doc("Get All Jokes in a Single Catagory")
class InsultCatagory(Resource):
    """Get All Jokes in a Single Catagory"""
    @api.doc(model=all_jokes_model)
    @api.param("category", "The category of the joke to be returned. To get a full list of categories, see the documentation for the `insult/categories` endpoint", choices=joke_categories)
    #!FIXME - @cache.cached(timeout=900) -  Does not Wortk via Swagger UI
    def get(self, category):
        
        """Get All Jokes in a Single Catagory"""
        if category in joke_categories:
            all_jokes_catagory = Jokester.get_all_jokes_from_catagory(category)
            joke_content = list()
            for joke in all_jokes_catagory:
                joke_content.append(joke["content"])
                # jokes_served()
            return {"All {} jokes".format(category): joke_content}, 200
        else:
            return {"error": "Invalid Catagory"}, 404


@api.route("insult")
@api.doc("Primary endpoint for interacting with the Insults Database. Allows for either randomize, catagorized or filtered insults to be returned with or without metadata.")
class InsultsAPI(Resource):
    """Insults API - Primary Endpoint"""

    #! Primary GET ENDPOINT - Insults
    # FIXME - Add Rate Limiting
    # @limiter.limit("3/day")
    @api.response(200, "<strong>Insults Found</strong> - Here's your joke(s)!")
    @api.doc(model=GET_fields)
    @api.response(
        400,
        " <strong>Invalid Explicit Filter</strong> - Please ensure you have set the explicit filter to either 'true' or 'false'",
    )
    @api.response(
        404,
        "<strong>Invalid Catagory or Explicit Filter</strong> - Each joke is assigned to a category based on content. To get a full list of categories, see the documentation for the `insult/categories` endpoint or <strong>No jokes found for that catagory with that explicit classification</strong> - Please try again with a different catagory or explicit filter",
    )
    @api.response(
        429,
        "<strong>Rate Limit Exceeded</strong> - Please wait 5 miniutes before trying again",
    )
    @api.expect(GET_parser)
    def get(self):
        
        args = request.args
        catagory = args.get("catagory")
        explicit = args.get("explicit")
        metadata = args.get("metadata")
        if type(explicit) == str:
            explicit = explicit.lower()
            if explicit == "true":
                explicit = True
            elif explicit == "true":
                explicit = False
            else:
                return {"error": "Invalid Explicit Filter"}, 400
        if type(metadata) == str:
            metadata = metadata.lower()
        if type(catagory) == str:
            catagory = catagory.lower()
        print(
            f"catagory: {catagory}, explicit settings: {explicit}, Include Metadata?: {metadata}"
        )
        if explicit is None and catagory is None:
            if metadata == "true":
                joke = Jokester.get_random_joke()
                return {"Yo Mama So...": joke}, 200
            else:
                joke = Jokester.get_random_joke()
                return {
                    "Yo Mama So...": joke["content"],
                    "id": joke["_id"]["$oid"],
                }, 200
        elif explicit is None:
            joke = Jokester.convert_insult(Jokester.get_categorized_joke(catagory))
            # NOTE - Checking to see if the DB Returns an Error - either No Joke In Catagory or Invalid Catagory

            if "error" in joke.keys():
                return joke, 400
            # NOTE - Checking to see if the DB Returns a joke - Joke found
            else:
                # NOTE - Checking to see if the user wants metadata - if so return the entire Insult Object
                if metadata == "true":
                    try:
                        return {"Yo Mama So...": joke}, 200
                    except Exception as e:
                        pass
                # NOTE - Path for No Metadata - Return only the content of the joke in desired catagory ,
                else:
                    try:
                        joke = Jokester.get_categorized_joke(catagory)
                        joke = Jokester.convert_insult(joke)
                        return {
                            "Yo Mama So...": joke["content"],
                            "id": joke["_id"]["$oid"],
                        }, 200
                    except Exception as e:
                        print(e)
                        pass

        elif catagory is None:
            if metadata == "true":
                joke = Jokester.get_censored_joke(explicit)
                if type(joke) == dict:
                    if "error" not in joke.keys():
                        return {"Yo Mama So...": joke}, 200
                    else:
                        print(e)
                        return joke, 404
            else:
                joke = Jokester.get_censored_joke(explicit)
                print(joke)
                return {
                    "Yo Mama So...": joke["content"],
                    "id": joke["_id"]["$oid"],
                }, 200

        else:
            if catagory in joke_categories:
                if explicit in acceptable_boolean_responses:
                    joke = Jokester.get_categorized_and_censored_joke(
                        catagory, explicit
                    )
                    if type(joke) == Insult:
                        joke = Jokester.convert_insult(joke)
                    if "error" not in joke.keys():
                        print("Both -A")
                        return {
                            "Yo Mama So...": joke["content"],
                            "id": joke["_id"]["$oid"],
                        }, 200
                    else:
                        return joke, 404
                else:
                    return joke, 400
            else:
                print("Both - Error 2")
                return {"error": "Invalid Catagory"}, 404

    # FIXME -  Rate Limiting
    # @jwt_required
    # @api.response(200, "Insult Added")
    # @api.response(401, "Unauthorized")
    # @api.response(403, "Banned User")
    # @api.response(400, "Bad Request")
    # @limiter.limit("1 per minute")
    # def post(self):
    #     identity = verify_jwt_in_request(locations=["headers"])
    #     if identity is not None:
    #         claims = get_jwt()
    #         user = User.objects.get(id=claims["id"])
    #         banned = SecurityGuard.is_user_banned(user["email"])
    #         try:
    #             if not banned:
    #                 content = request.get("content")
    #                 category = request.json["category"]
    #                 explicit = request.get["explicit"]
    #                 date = str(now.to_datetime_string())
    #                 Insult(
    #                     content=content, explicit=explicit, added_on=date, added_by=user
    #                 ).save()
    #                 insult = Insult.objects.all()
    #                 return {"Status": "Insult Added"}, 201
    #         except BannedUserError:
    #             raise UnauthorizedError(
    #                 "Adding this content is not allow, the user has been banned"
    #             )
    #     else:
    #         raise InvaildTokenError()


#! ✅ GET Endpoint - Get Specific Insult
@api.response(200, "<strong>Insults Found</strong> - Here's your joke!")
@api.response(404, "<strong>Insult with provided ObjectId not found</strong>. Please check ID and resubmit request.")
@api.response(400, "<strong>Invalid ObjectId</strong> - Please that ID is valid and resubmit request.")
@api.doc(model=GET_fields)
@api.doc("Endpoint to get the full insult with metadata for a specific joke returned.")
@api.doc(params={"id": "This is the UUID for a Specific Joke"})
@api.route("insult/<string:id>")
class SpecificInsult(Resource):
    def get(self, id):
        try:
            jokes = Jokester.get_specific_joke(id)
            print(type(jokes))
            if type(jokes) is None or len(jokes) == 0:
                return {"error": "Insult with provided ObjectId not found. Please check ID and resubmit request."}, 404
            else:
                return {"Yo Mama So...": jokes}, 200
        except TypeError:
            return {"error": "Insult with provided ObjectId not found. Please check ID and resubmit request."}, 404
        except bson.errors.InvalidId:
            return {"error": "Invalid ObjectId"}, 400
