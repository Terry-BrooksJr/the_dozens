import json
import os
from unittest import TestCase

import flask
import pymongo
import requests
from pytest_check import check

from utils.errors import APINotRunningError, DBConnectionError

URI = os.getenv("MONGODB_URI")
API_URL = "http://localhost:7000"
client = pymongo.MongoClient(URI)

try:
    connected = True if client.admin.command("ping")["ok"] == 1 else False
except pymongo.errors.ServerSelectionTimeoutError as e:
    raise DBConnectionError


class TestInsultEndpoints(TestCase):
    """This class is responsible for testing the insult endpoints."""

    def test_get_insults(self):
        """This method tests the /insults endpoint without Metadata."""
        if connected:
            response_without_filter_value = requests.get(f"{API_URL}/insult")
            response_with_filter_value = requests.get(
                f"{API_URL}/insult?metadata=false"
            )
            resp_json = json.loads(response_without_filter_value.text)
            with check:
                assert response_without_filter_value.status_code == 200
                assert response_with_filter_value.status_code == 200
                assert response_without_filter_value.json() is not None
                assert response_with_filter_value.json() is not None
                assert len(resp_json.keys()) == 2
        else:
            raise APINotRunningError

    def test_get_insults_with_metadata(self):
        """This method tests the /insults endpoint with Metadata."""
        if connected:
            response = requests.get(f"{API_URL}/insult?metadata=true")
            resp_json = json.loads(response.text)
            resp_json_joke = resp_json["Yo Mama So..."].keys()
            with check:
                assert response.status_code == 200
                assert response.json() is not None
                assert len(resp_json_joke) == 7
        else:
            raise APINotRunningError

    def test_catagroized_joke(self):
        """This method tests the /insults endpoint without Metadata."""
        if connected:
            response_without_filter_value = requests.get(
                f"{API_URL}/insult?catagory=test-catagory"
            )
            response_with_filter_value = requests.get(
                f"{API_URL}/insult?catagory=test-catagory&metadata=false"
            )
            resp_json = json.loads(response_without_filter_value.text)
            resp_json_joke = resp_json["Yo Mama So..."]

            with check:
                assert response_without_filter_value.status_code == 200
                assert response_with_filter_value.status_code == 200
                assert response_without_filter_value.json() is not None
                assert response_with_filter_value.json() is not None
                assert len(resp_json.keys()) == 2
        else:
            raise APINotRunningError

    def test_catagroized_joke_with_metadata(self):
        """This method tests the /insults endpoint with Metadata."""
        if connected:
            response_with_filter_value = requests.get(
                f"{API_URL}/insult?catagory=test-catagory&metadata=true"
            )
            resp_json = json.loads(response_with_filter_value.text)
            resp_json_joke = resp_json["Yo Mama So..."]

            with check:
                assert response_with_filter_value.status_code == 200
                assert resp_json_joke["category"] == "test-catagory"
                assert response_with_filter_value.json() is not None
                assert len(resp_json_joke.keys()) == 7
        else:
            raise APINotRunningError

    def test_censored_joke(self):
        """This method tests the /insults endpoint with Metadata."""
        if connected:
            response_with_filter_value = requests.get(
                f"{API_URL}/insult?explicit=true&metadata=true"
            )
            resp_json = json.loads(response_with_filter_value.text)
            resp_json_joke = resp_json["Yo Mama So..."]
            with check:
                assert response_with_filter_value.status_code == 200
                assert response_with_filter_value.json() is not None
                assert resp_json_joke["explicit"] == True
                assert len(resp_json_joke.keys()) == 7
        else:
            raise APINotRunningError

    def test_get_specific_joke(self):
        """This method tests the find specific joke functionaliy."""
        if connected:
            response_with_filter_value = requests.get(
                f"{API_URL}/insult/63d27ab5afe647264a1b8bbc"
            )
            resp_json = json.loads(response_with_filter_value.text)
            resp_json_joke = resp_json["Yo Mama So..."]
            with check:
                assert response_with_filter_value.status_code == 200
                assert response_with_filter_value.json() is not None
                assert resp_json_joke["_id"]["$oid"] == "63d27ab5afe647264a1b8bbc"
        else:
            raise APINotRunningError


class TestInvaildInputs(TestCase):
    """This class is responsible for testing the insult endpoints with invalid inputs."""

    def test_invalid_explicit_filter(self):
        if connected:
            response = requests.get(f"{API_URL}/insult?explicit=YES")
            resp_json = json.loads(response.text)
            with check:
                assert response.status_code == 400
                assert resp_json["error"] == "Invalid Explicit Filter"

    def test_invaild_catagory(self):
        if connected:
            response = requests.get(f"{API_URL}/insult?catagory=hot")
            payload = response.json()
            with check:
                assert response.status_code == 400
                assert payload["error"] == "Invalid catagory"

    def test_invaild_id(self):
        if connected:
            response = requests.get(f"{API_URL}/insult/123")
            payload = response.json()
            with check:
                assert response.status_code == 400
                assert payload["error"] == "Invalid ObjectId"

    def test_not_found_id(self):
        if connected:
            response = requests.get(f"{API_URL}/insult/63cd3facc56e9a2922344f93")
            payload = response.json()
            with check:
                assert "error" in payload.keys()
