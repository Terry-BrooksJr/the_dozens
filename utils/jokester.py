"""
This Module is responsible for all the actions that occur in the /insults endpoint, and the functions that are called by the /insults routes.
"""
import json
import random
import os
import bson.json_util as json_util
from bson import ObjectId
from pymongo import MongoClient

from database.models import Insult

client = MongoClient(os.getenv("MONGODB_URI"))
acceptable_boolean_responses = ["true", "false", True, False]


class Jokester:
    """This utility class is responsible for all the actions that occur in the /insults endpoint, and the functions that are called by the /insults routes."""

    @staticmethod
    def parse_json(data):
        """This function will parse the json data into a python dictionary"""
        return json.loads(json_util.dumps(data))

    @staticmethod
    def dict_to_(lst):
        """"""
        res_dct = {lst[i]: lst[i + 1] for i in range(0, len(lst), 2)}
        return res_dct

    @staticmethod
    def get_random_joke():
        randomized_pipeline = [{"$sample": {"size": 1}}]
        randomized_joke = str()
        insult = Insult.objects().aggregate(randomized_pipeline)
        for doc in insult:
            randomized_joke = Jokester.parse_json(doc)
        return randomized_joke

    @staticmethod
    def get_censored_joke(explicit_settings):
        if type(explicit_settings) == bool:
            try:
                # censored_joke = None
                insult = client["InsultVault"]["insults"].aggregate(
                    [
                        {"$match": {"explicit": explicit_settings}},
                        {"$sample": {"size": 1}},
                    ]
                )
                for doc in insult:
                    censored_joke = Jokester.parse_json(doc)
                    return censored_joke
            except Exception as e:
                print(f"error path - {e}")
                return {"error": "No Jokes Found With that explicit classification"}
        else:
            return {"error": "Invalid Explicit Filter"}

    @staticmethod
    def get_categorized_joke(catagory_selection):
        """This function will return a random joke from the catagory selected"""
        catagory_selection = catagory_selection
        filter = {"category": catagory_selection}
        insult = Insult.objects().filter(category=catagory_selection)
        insult_ranger = client["InsultVault"]["insults"].find(filter=filter)
        results = list()
        if catagory_selection not in Jokester.get_catagories():
            return {"error": "Invalid catagory"}
        try:
            upper_range = len(list(insult_ranger))
            randomizer = random.randint(0, upper_range - 1)
            for joke in insult:
                results.append(joke)
            return results[randomizer]
        except (IndexError, ValueError, Exception) as e:
            return {"error": "No jokes found for that catagory"}

    @staticmethod
    def get_catagories():
        """This function will return a list of all the catagories"""
        catagories = Insult.objects()
        catagory_list = list()

        for cat in catagories:
            if cat["category"] != "test" or cat["category"] != "test - catagory":
                if cat["category"] not in catagory_list:
                    catagory_list.append(cat["category"])
            sorted_list = sorted(catagory_list)
        return sorted_list

    @staticmethod
    def get_all_jokes_from_catagory(catagory):
        """This function will return all the jokes from a catagory""" ""
        if catagory not in Jokester.get_catagories():
            return {"error": "No jokes found for that catagory"}, 404
        else:
            filter = {"category": catagory}
            result = client["InsultVault"]["insults"].find(filter=filter)
            results = list()
            for joke in result:
                results.append(joke)
            return results

    #! ✅ GET Specific Joke based on ID
    @staticmethod
    def get_specific_joke(id):
        filter = {"_id": ObjectId(str(id))}
        results = client["InsultVault"]["insults"].find(filter=filter)
        for joke in results:
            return Jokester.parse_json(joke)

    @staticmethod
    def get_categorized_and_censored_joke(catagory_selection, explicit_settings):
        """This function will return a random joke from the catagory selected and based on the explicit settings"""
        filter = {"category": catagory_selection, "explicit": explicit_settings}
        insult = Insult.objects().filter(category=catagory_selection)
        insult_ranger = client["InsultVault"]["insults"].find(filter=filter)
        results = list()
        try:
            upper_range = len(list(insult_ranger))
            randomizer = random.randint(0, upper_range - 1)
            for joke in insult:
                results.append(joke)
            return results[randomizer]
        except (IndexError, ValueError, Exception) as e:
            return {
                "error": "No jokes found for that catagory with that explicit classification"
            }

    @staticmethod
    def convert_insult(insult):
        """This function will convert the insult into a dictionary"""
        if type(insult) != dict:
            insult = insult.to_json()
            insult = json.loads(insult)
        return insult
