"""
 It is responsible for initializing the Flask app, the database, the API, and the routes.
"""
import os
from flask import Flask, send_from_directory

from config import Config
from database.db import initialize_db
from resources import api
from extensions import bcrypt, jwt, cache
#!SECTION Instantaites the Flask app


#!SECTION Initialize the API Endpoints and Plugins

def create_api():
    """This function is responsible for creating the API."""
    app = Flask(__name__)
    app.config.from_object(Config)
    register_extenstions(app)
    api.init_app(app)
    initialize_db(app)
    return app
    
#TODO: Add the Logging function

def register_extenstions(app):
    """This function is responsible for registering the extensions with the Flask app."""
    bcrypt.init_app(app)
    jwt.init_app(app)
    cache.init_app(app)

