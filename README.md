# Yo' Mama - The Roast API

[![linting: pylint](https://img.shields.io/badge/linting-pylint-yellowgreen)](https://github.com/PyCQA/pylint) [![Codacy Badge](https://app.codacy.com/project/badge/Grade/3eb1b819338e4b5eb8ca2327c62b46dd)](https://www.codacy.com/gh/Terry-BrooksJr/The-Dozens---The-Roast-API/dashboard?utm_source=github.com&utm_medium=referral&utm_content=Terry-BrooksJr/The-Dozens---The-Roast-API&utm_campaign=Badge_Grade)

This project shows one of the possible ways to implement RESTful API server.

There are implemented two models: User and Insult.

  While there are class methods associated with the model's classes, the majaority of methods/functions have been abstracted away via static methods of specialized Utility Classes in the `/utils` directory. 

  Design Choice was made to ensure separation of concerns and modularity:
  - Util Classes are the Actions. 
  - Model Classes  are the Structure.

Main libraries used:

1. [Flask](https://flask.palletsprojects.com/en/2.2.x/) - Micro-Framerwork
2. [Flask-JWT-Extended](https://flask-jwt-extended.readthedocs.io/en/stable/) - for handling Bearer Token Provision and Validation for Creation, Update operations.
3. [Flask-RESTx](https://flask-restx.readthedocs.io/en/latest/)- restful API library.
4. [Flask-Mongoengine](http://mongoengine.org/) - adds support for MongoDB ORM.
5. [Flask-Bcrypt](https://flask-bcrypt.readthedocs.io/en/1.0.1/) - supports the user registration system for users who want to contribute jokes
6. [Flask-Limiter](https://flask-limiter.readthedocs.io/en/stable/) - Used for rate limiting the number of requests to endpoints.
8. [Flask-Caching](https://flask-caching.readthedocs.io/en/latest/) - Used to implement the cache constraint of RESTful design concept. 

## Project Structure:

```
.
├── Pipfile
├── Pipfile.lock
├── README.md
├── app.py
├── config.py
├── database
│   ├── __init__.py
│   ├── db.py
│   ├── init_jokes.json
│   └── models.py
├── makefile
├── pytest.ini
├── requirements.txt
├── resources
│   ├── __init__.py
│   ├── auth.py
│   ├── insult.py
│   └── status.py
├── test
│   ├── __init__.py
│   ├── test_auth.py
│   ├── test_db.py
│   ├── test_jokester.py
│   └── test_utils.py
├── utils
│   ├── __init__.py
│   ├── administrator.py
│   ├── errors.py
│   ├── gatekeeper.py
│   └── jokester.py
└── wsgi.py
```
## Navigating The Code:

- resources - holds all endpoints.
- app.py - flask application initialization.
- tests - all testing related to the API
- utils - Collection of Global utility Classes and functions
- database - All things related to the primary Database - MongoDB
- wsgi.py - App entrypoint

## Running

1. Clone repository.
2. run  `pip install -r requirements.txt`
3. Start server by running `gunicorn wsgi:app`

## Usage
**Note** - This API is currently in BETA. Any Issues Please email [Terry@BrooksJr.com](mailto:Terry@BrooksJr.com)

## POST endpoints - Coming Soon 

POST https://api.yo-mama-so.fun/signup

REQUEST

```json
{
  "email": "sample@email.com",
  "password": "myPassWord"
}
```

POST Keys(Planned Enhancement):

- `email`: Valid Email Address
- `password` : Any combination Of **7 or More** ASCII Character.
  RESPONSE

```json
{
  "id": "639908d09e3d57d4baa655d4"
}
```

Returns BSON id - You **DO NOT** need to retain this ID.

POST https://api.yo-mama-so.fun/token


## GET Endpoints
**Unless otherwise noted**, all GET requests to the `/insult` endpoint return a JSON object with two keys: 


| Key           | Explanation                          | Example                                                                |
|---------------|--------------------------------------|------------------------------------------------------------------------|
| Yo Mama So... | This the Content of the Joke         | Yo momma is so fat... she went to the movies and sat next to everyone. |
| id            | This is the UUID for a Specific Joke | 63ab75120ed9e7d0f661dba5                                               |


### GET https://api.yo-mama-so.fun/insult
 - Returns 1 random uncategorized, uncensored joke
    - Example `200` Response
```json
{
	"Yo Mama So...": "Yo momma is so poor... I walked in her house and stepped on a cigarette, and your mom said, 'Who turned off the lights?'",
	"id": "63ab75120ed9e7d0f661dbfd"
}
```

### GET https://api.yo-mama-so.fun/insult/{category}
 - Returns 1 random categorized, uncensored joke
   - Parameters: 

| Parameter | Explanation                                                                                                                                        | Example |
|-----------|----------------------------------------------------------------------------------------------------------------------------------------------------|---------|
| category  | Each joke is assigned to a category based on content. To get a full list of categories, see the documentation for the `insult/categories` endpoint | poor     |

  - Example `200` Response:

```json
{
	"Yo Mama So...": "Yo momma is so fat... she went to the movies and sat next to everyone.",
	"id": "63ab75120ed9e7d0f661dba5"
}
```

### GET https://api.yo-mama-so.fun/insult/{id}
 - Returns a specific joke based on id with 7 keys: 

 | Key      | Explanation                                                                                                                                                                                         | Example                                                                |
|----------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------|
| _id      | A nested Object with a single nested key `$oid` a string representation of the MongoDB ObjectID.                                                                                                    | { "$oid": "63ab75120ed9e7d0f661dba5"}                                  |
| content  | This the Content of the Joke                                                                                                                                                                        | Yo momma is so fat... she went to the movies and sat next to everyone. |
| category | Each joke is assigned to a category based on content. To get a full list of categories, see the documentation for the `insult/categories` endpoint                                                  | fat                                                                    |
| added_by | The Initials of the contributor and a unique identifier who added the joke to the API                                                                                                                | TB12345                                                                |
| added_on | String representation of the date the joke was logged to the API database. Format: YYYY-MM-DD. (Auto Set By Server)                                                                                 | 2022-12-14                                                             |
| status   | The status of the Joke in the API. Valid Status include ['Active', 'Pending', 'Under Review', 'Deleted'] <sub>If a status is 'Deleted' please open and issue and provide the ID</sub>               | Active                                                                 |
| explicit  | Classification of the Joke to determine if it's content is considered explicit or otherwise NSFW. <sub>If you feel a joke needs to be reclassified, please report the joke and provide the id</sub> | false                                                                  |

   - Parameters: 

| Parameter | Explanation                                                                                                                                        | Example |
|-----------|----------------------------------------------------------------------------------------------------------------------------------------------------|---------|
| id  | This is the UUID for a Specific Joke | 63ab75120ed9e7d0f661dba5     |

  - Example `200` Response:

```json
{
	"Yo Mama So...": {
		"_id": {
			"$oid": "63ab75120ed9e7d0f661dba5"
		},
		"content": "Yo momma is so fat... she went to the movies and sat next to everyone.",
		"category": "fat",
		"added_by": "Developer",
		"added_on": "2022-12-14",
		"status": "Active",
		"explicit": false
	}
}
```

### GET https://api.yo-mama-so.fun/insult/categories
- Returns a list of available joke categories 
  - Example of `200` Response:
```json
{
	"Available Joke Categories": [
		"bald",
		"daddy - old",
		"daddy - stupid",
		"fat",
		"general",
		"hairy",
		"lazy",
		"nasty",  
		"old",
		"poor",
		"short",
		"skinny",
		"snowflake",
		"stupid",
		"tall",
		"ugly"
	]
}

```
### GET https://api.yo-mama-so.fun/insult/{category}
- Returns a list of all joke categories in a single category
  - Example of `200` Response:
  ```

## DELETE & PATCH Operations Are Not Supported.

<sub>Feel free to open an issue or submit a PR</em>

## Additional Documentation

- [Postman](https://www.postman.com/terryabrooksjr/workspace/the-roast-api-yo-mama-jokes)
- [Swagger](https://api.yo-mama-so.fun)`
