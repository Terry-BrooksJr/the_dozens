import os


class Config(object):
    """This class is responsible for setting the configuration for the app."""
    DEBUG = True
    SECRET_KEY = os.getenv(key="SECRET_KEY")
    BUNDLE_ERROR = True
    ENV = ("Development",)
    JWT_TOKEN_LOCATION = ("headers",)
    JWT_HEADER_NAME = ("Authorization",)
    JWT_SECRET_KEY = (os.getenv("JWT_SECRET_KEY"),)
    SECRET_KEY = os.getenv("SECRET_KEY")
    CACHE_TYPE = "RedisCache"
    CACHE_DEFAULT_TIMEOUT = 300
    MONGODB_SETTINGS = {"host": os.getenv("MONGODB_URI"), "alias": "prod-default"}
    CACHE_REDIS_URL = os.getenv("REDIS_CACHE_URL")