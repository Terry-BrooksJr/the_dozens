import werkzeug.exceptions
from flask_restx import Api

import os

from utils.errors import (
    BannedUserError,
    EmailAlreadyExistsError,
    InternalServerError,
    InvaildTokenError,
    NoJokesFoundError,
    ResourceNotDFoundError,
    UnauthorizedError,
    UserDoesNotExist,
    ValidationError,
    errors,
)

from .auth import api as AuthNS
from .insult import api as InsultNS
from .status import api as StatusNS

api = Api(
    errors=errors,
    # url_scheme="https",
    version="Beta",
    title="Yo Mama - The Roast API",
    description="The dozens is a game of verbal combat, played mostly by African Americans on street corners, in barbershops, wherever. It is designed to teach participants to maintain control and keep cool under adverse circumstances. <br><br> Clarence Major, in <em>'Juba to Jive: A Dictionary of African American Slang</em>,' defines the Dirty Dozens as a very elaborate verbal rhyming game traditionally played by black boys, in which the participants insult each other's relatives -- in 12 censures -- especially their mothers. The object of the game is to test emotional strength. <br><br> A new generation of comics and their adoring fans refer to the game as 'snaps'. Richard Majors and Janet Mancini Billson, in their book, <em>'Cool Pose: The Dilemmas of Black Manhood in America</em>,' wrote that playing the dozens prepares black men for socio- economic problems they may later face and facilitates their search for masculinity, pride, status, social competence and expressive sexuality.<br><br> This API is in a BETA Launch. Please report any issues via a GH iSSUE or email me at [Terry@BrooksJr.com](mailto:Terry@BrooksJr.com).",
    contact="projects@brooksjr.com",
)

# api.add_namespace(StatusNS, "/")
# api.add_namespace(AuthNS, "/")
api.add_namespace(InsultNS, "/")


@api.errorhandler(ResourceNotDFoundError)
def handle_no_result_exception(error):
    '''Return a custom not found error message and 404 status code'''
    return {"message": "Yea...No...That resource does not exisit. Try again!", "details": error.message}, 404

@api.errorhandler(Exception)
def handle_root_exception(error):
    """Return a custom message and 400 status code"""
    return {
        "message": f"Looking for an Insult? How bout yo So Stupid...You can't even make a vaild HTTP request. But Seriousaly, our servers don't know what the fuck to do with that request",
        "details": error,
    }, 400
