from app import create_api
from flask import send_from_directory
import os
app = create_api()


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                          'favicon.ico',mimetype='image/vnd.microsoft.icon')
if __name__ == "__main__":
    app.run()